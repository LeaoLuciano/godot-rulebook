use super::{
    component::*,
    world::{World},
};
use godot::{
    engine::{Script},
    prelude::*,
};

#[derive(GodotClass)]
#[class(base=Node2D)]
pub struct Entity2D {
    world: Option<Gd<World>>,
    #[base]
    base: Base<Node2D>,
}

#[godot_api]
impl Entity2D {
    #[func]
    pub fn get_component(&self, script: Gd<Script>) -> Option<Gd<Component>> {
        let mut component = Array::<Gd<Component>>::new();

        let nodes = self.get_children(false);
        for node in nodes.iter_shared() {
            // godot_print!("{} == {}", node, script);
            // if node.get_script().to::<Gd<Script>>() == script {
                if let Ok(node_script) = node.get_script().try_to::<Gd<Script>>() {
                    if node_script == script {
                        component.push(node.cast::<Component>());
                    }
                }
            }

        if component.len() > 1 {
            godot_warn!("Entity with multiple properties of the same type.");
        }
        else if component.len() == 1 {
            return Some(component.get(0));
        }

        None
    }
}

fn get_simulation(node: Gd<Node>) -> Option<Gd<World>> {
    if let Some(parent) = node.get_parent() {
        if let Some(parent) = parent.share().try_cast::<World>() {
            return Some(parent.share());
        } else if let Some(parent) = parent.share().try_cast::<Entity2D>() {
            if let Some(parent) = parent.bind().world.as_ref() {
                return Some(parent.share());
            }
        }
    }

    None
}

#[godot_api]
impl NodeVirtual for Entity2D {
    fn init(base: Base<Node2D>) -> Self {
        Self {
            world: None,
            base,
        }
    }

    fn enter_tree(&mut self) {
        self.world = get_simulation(self.base.share().upcast());

        if let Some(world) = &self.world {
            let entity = self.base.share().cast::<Entity2D>();
            world.share().bind_mut().add_entity(entity);
        }
    }

    fn exit_tree(&mut self) {
        if let Some(world) = &self.world {
            let entity = self.base.share().cast::<Entity2D>();
            world.share().bind_mut().remove_entity(entity);
        }

        self.world = None;
    }
}
