use godot::prelude::*;

use super::entity::Entity2D;



#[derive(GodotClass)]
#[class(base=Node)]
pub struct Component {
    #[base]
    base: Base<Node>,
}


#[godot_api]
impl NodeVirtual for Component {
    fn init(base: Base<Node>) -> Self {
        Self {
            base,
        }
    }

    fn enter_tree(&mut self) {
        if let Some(parent) = self.base.get_parent() {
            if let None = parent.try_cast::<Entity2D>() {
                godot_warn!("Component needs to be child of a entity");
            }
        }
    }
}

