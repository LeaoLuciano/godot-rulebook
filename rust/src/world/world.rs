use godot::{engine::Script, prelude::*};

use super::entity::Entity2D;

#[derive(GodotClass)]
#[class(base=Node)]
pub struct World {
    entities: Dictionary,
    #[base]
    base: Base<Node>,
}

#[godot_api]
impl World {
    pub fn add_entity(&mut self, entity: Gd<Entity2D>) {
        if let Some(_) = self.entities.get(entity.share()) {
            godot_warn!("Bug: Entity already added");
        }
        self.entities.insert(entity.share(), ());
    }

    pub fn remove_entity(&mut self, entity: Gd<Entity2D>) {
        if let None = self.entities.get(entity.share()) {
            godot_warn!("Bug: Entity already removed");
        }
        self.entities.remove(entity.share());
    }

    #[func]
    pub fn query(&self, components: Array<Gd<Script>>) -> Array<Gd<Entity2D>> {
        let mut result = Array::new();

        'entities: for entity in self.entities.keys_array().iter_shared() {

            match entity.try_to::<Gd<Entity2D>>() {
                Ok(entity) => {

                    for component in components.iter_shared() {

                        let component = component;
                        
                        if let None = entity.bind().get_component(component) {
                            continue 'entities;
                        }
                    }
                    result.push(entity);
                }
                Err(_) => godot_warn!("Bug: not a Entity"),
            }
        }

        result
    }
}

#[godot_api]
impl NodeVirtual for World {
    fn init(base: Base<Node>) -> Self {
        World {
            entities: Dictionary::new(),
            base,
        }
    }

    fn ready(&mut self) {}
}
