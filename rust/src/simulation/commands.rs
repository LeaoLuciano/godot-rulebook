use godot::engine::{Engine, Script};
use godot::prelude::*;

use crate::rule::action::{Action, Attribute, ATTRIBUTE_CLASS_NAME};

use crate::world::world::World;

use super::simulation::{Simulation};

// Deriving GodotClass makes the class available to Godot
#[derive(GodotClass)]
#[class(base=RefCounted)]
pub struct Commands {
    enabled: Enabled,
    simulation: Option<Gd<Simulation>>,
    #[base]
    base: Base<RefCounted>,
}

#[derive(PartialEq, Eq)]
pub enum Enabled {
    ActionsAndRules,
    EntireSimulation,
    Disabled,
}

#[godot_api]
impl Commands {
    #[func]
    fn apply_action(&self, action: Gd<Action>) -> () {
        if let Enabled::Disabled = self.enabled {
            godot_error!("Cannot apply action: node without permission");
            return;
        }

        let mut binding = action.share();
        let mut attributes: Vec<(String, Gd<Script>)> = Vec::new();

        {
            let mut action2 = binding.bind_mut();


            for property in action2.get_property_list().iter_shared() {
                let property_name = property.get("name").unwrap().to::<String>();
                let mut class = property.get("class_name").unwrap().to::<StringName>();

                
                if class == "".into() {
                    let obj = action2.get(property_name.clone().into());
                    if obj.is_nil() {
                        continue;
                    }
                    if let Ok(obj) = obj.try_to::<Gd<Object>>() {
                        class = obj.get_class().into();
                    }
                }

                
                if class == ATTRIBUTE_CLASS_NAME.into() {
                    let script = action2
                        .get(property_name.clone().into())
                        .to::<Gd<Object>>()
                        .get_script()
                        .to::<Gd<Script>>();

                    attributes.push((property_name, script));
                }
            }

            for (property, script) in attributes {
                let property = action2.get(property.into()).to::<Gd<Attribute>>();
                if let Some(_) = action2.attributes.insert(script, property) {
                    godot_error!("Action cannot have multiple attributes of the same type");
                }
            }
        }

        let rulebook = match &self.simulation {
            Some(simulation) => simulation.bind().get_rulebook(),
            None => {
                godot_error!("There's no current simulation");
                return;
            }
        };

        let rulebook = match rulebook {
            Some(rulebook) => rulebook,
            None => {
                godot_error!("Simulation doesn't have a Rulebook child");
                return;
            }
        };

        rulebook.bind().apply_rules(action, self.simulation.as_ref().unwrap().share());
    }

    // #[func]
    // pub fn add_rule() {

    // }

    // #[func]
    // pub fn remove_rule() {

    // }

    // pub(super) fn enable(&mut self, simulation: Gd<Simulation>, mode: Enabled) {
    //     if Enabled::Disabled != self.enabled {
    //         godot_error!("Bug: Commands node already enabled");
    //     }

    //     self.simulation = Some(simulation);
    //     self.enabled = mode;
    // }

    // pub(super) fn disable(&mut self) {
    //     if let Enabled::Disabled = self.enabled {
    //         godot_error!("Bug: Commands node already disabled");
    //     }

    //     self.simulation = None;
    //     self.enabled = Enabled::Disabled;

    // }

    // #[func]
    // pub fn enabled(&self) -> Enabled {
    //     return self.enabled;
    // }

    #[func]
    pub fn get_world(&self) -> Option<Gd<World>> {
        if Enabled::Disabled == self.enabled {
            godot_error!("Cannot get world: node without permission");
            return None;
        }

        match &self.simulation {
            Some(simulation) => simulation.bind().get_world(),
            None => {
                godot_error!("There's no current simulation");
                None
            }
        }
    }

    #[func]
    pub fn set_simulation(&mut self, simulation: StringName) {
        for node in Engine::singleton()
            .get_main_loop()
            .unwrap()
            .cast::<SceneTree>()
            .get_nodes_in_group("RulebookSimulation".into())
            .iter_shared()
        {
            if node.get_name() == simulation {
                self.simulation = Some(node.cast::<Simulation>());
                return;
            }
        }

        let node = match Engine::singleton()
            .get_main_loop()
            .unwrap()
            .cast::<SceneTree>()
            .get_first_node_in_group("RulebookSimulation".into())
        {
            Some(node) => node.cast::<Simulation>(),
            None => {
                godot_error!("There is no Simulation node in tree");
                return;
            }
        };

        self.simulation = Some(node.cast::<Simulation>());
    }
}

#[godot_api]
impl RefCountedVirtual for Commands {
    fn init(base: Base<Self::Base>) -> Self {
        let mut commands = Self {
            enabled: Enabled::EntireSimulation,
            simulation: None,
            base,
        };
        commands.set_simulation("".into());
        commands
    }
}
