use godot::{prelude::*};

use crate::{rule::rulebook::Rulebook, world::world::World, util::get_child_from};


#[derive(GodotClass)]
#[class(base=Node)]
pub struct Simulation {
    #[base]
    base: Base<Node>,
}

#[godot_api]
impl Simulation {

    #[func]
    pub fn get_rulebook(&self) -> Option<Gd<Rulebook>>{
        get_child_from::<Rulebook>(self.base.share())
    }

    #[func]
    pub fn get_world(&self) -> Option<Gd<World>>{
        get_child_from::<World>(self.base.share())
    }

}

#[godot_api]
impl NodeVirtual for Simulation {
    fn init(base: Base<Node>) -> Self {
        Simulation {
            base,
        }
    }

    fn get_configuration_warnings(&self) -> PackedStringArray {
        let mut warnings = PackedStringArray::new();


        if self.get_rulebook().is_none() {
            warnings.push(GodotString::from("This node doesn't have a Rulebook node"));
        }

        if self.get_world().is_none() {
            warnings.push(GodotString::from("This node doesn't have a World node"));
        }

        return warnings;
    }

    
    
    fn enter_tree(&mut self) {
        self.add_to_group("RulebookSimulation".into(), false);
    }
}
