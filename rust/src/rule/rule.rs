use godot::{
    engine::{Engine, Script},
    prelude::*,
};

use crate::simulation::{simulation::Simulation};

use super::action::{Action, Attribute};

#[derive(GodotClass)]
#[class(base=Node)]
pub struct Rule {
    attributes: Dictionary,
    #[base]
    base: Base<Node>,
}

#[godot_api]
impl Rule {
    // #[func]
    // pub fn apply(&mut self, mut _world: Gd<World>) -> () {
    //     godot_error!("Virtual method not implemented");
    // }

    pub fn _apply(&mut self, mut action: Gd<Action>, simulation: Gd<Simulation>) {
        let mut has_apply = false;

        for method in self.get_method_list().iter_shared() {
            if method.get("name").unwrap().to::<String>() == "apply" {
                has_apply = true;
                break;
            }
        }

        if !has_apply {
            godot_error!("Rule don't have a apply method");
            return;
        }

        let action_attr_types = action.bind().attributes.keys_array();

        for attr_type in self.attributes.keys_array().iter_shared() {
            if !action_attr_types.contains(&attr_type) {
                return;
            }
        }

        let default_attributes = self.attributes.duplicate_shallow();

        for (attr_type, action_attr) in action.bind().attributes.iter_shared() {
            if let Some(attr_name) = self.attributes.get(attr_type.clone()) {
                self.set(attr_name.to::<String>().into(), action_attr);
            }
        }

        self.call("apply".into(), &[Variant::from(simulation)]);

        let mut new_attrs : Vec<(String, Variant)>= Vec::new();

        for attr_type in action.bind().attributes.keys_array().iter_shared() {
            if let Some(attr_name) = self.attributes.get(attr_type.clone()) {
                let attr = self.get(attr_name.to::<String>().into());
                new_attrs.push((attr_name.to::<String>(), attr));

            }
        }

        for (name, value) in new_attrs {

            action.bind_mut().set(name.into(), value);
        }

        self.attributes = default_attributes;
    }

    fn update_attributes(&mut self) {

        let mut attrs: Vec<(Gd<Script>, String)> = Vec::new();

        for property in self.get_property_list().iter_shared() {
            let name = property.get("name").unwrap().to::<String>();
            let mut class = property.get("class_name").unwrap().to::<StringName>();

            if class == "".into() {
                let obj = self.get(name.clone().into());
                if obj.is_nil() {
                    continue;
                }
                if let Ok(obj) = obj.try_to::<Gd<Object>>() {
                    class = obj.get_class().into();
                }
            }

            if class == "Attribute".into() {
                let attr = self.get(name.clone().into()).to::<Gd<Attribute>>();
                let attr_type = attr.bind().get_script().to::<Gd<Script>>();

                attrs.push((attr_type, name));
            }
        }

        for (attr_type, attr_name) in attrs {
            if let Some(_) = self.attributes.insert(attr_type, attr_name) {
                godot_error!("Rule cannot query multiple attributes of the same type");
            }
        }
    }
}

#[godot_api]
impl NodeVirtual for Rule {
    fn init(base: Base<Self::Base>) -> Self {
        Self {
            attributes: Dictionary::new(),
            base,
        }
    }

    fn ready(&mut self) {
        if Engine::singleton().is_editor_hint() {
            return;
        }

        self.update_attributes();
    }

    fn to_string(&self) -> GodotString {
        format!("{:?}", self.attributes.values_array()).into()
    }
}
