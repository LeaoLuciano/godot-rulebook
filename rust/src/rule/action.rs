use godot::{
    prelude::*, engine::{Script, RegEx},
};

#[derive(GodotClass)]
#[class(base=RefCounted)]
pub struct Action {
    pub attributes: Dictionary,
    #[base]
    base: Base<RefCounted>,
}

#[derive(GodotClass)]
#[class(base=RefCounted)]
pub struct Attribute {
    #[base]
    base: Base<RefCounted>,
}

pub const ATTRIBUTE_CLASS_NAME: &str = "Attribute";

#[godot_api]
impl Action {
}

#[godot_api]
impl RefCountedVirtual for Action {
    fn init(base: Base<Self::Base>) -> Self {
        Self {
            base, 
            attributes: Dictionary::new(),
        }
    }

    fn to_string(&self) -> GodotString {
        format!("{:?}", self.attributes.values_array()).into()
    }
}

#[godot_api]
impl RefCountedVirtual for Attribute {
    fn init(base: Base<Self::Base>) -> Self {
        Self {
            base, 
        }
    }

    fn to_string(&self) -> GodotString {
        let source = self.get_script().to::<Gd<Script>>().get_source_code();
        let mut regex = RegEx::new();
        regex.compile("class_name (?<name>\\w+)".into());


        format!("{:?}", regex.search(source, 0, -1).unwrap().get_string(Variant::from("name"))).into()
    }

}
