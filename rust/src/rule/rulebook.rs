

use godot::prelude::*;



use crate::simulation::{simulation::Simulation};

use super::{rule::Rule, action::Action};

#[derive(GodotClass)]
#[class(base=Node)]
pub struct Rulebook {
    #[base]
    base: Base<Node>,
}

#[godot_api]
impl Rulebook {
    pub(crate) fn apply_rules(&self, action: Gd<Action>, simulation: Gd<Simulation>) {
        let rules = self.get_children(false);
        // let rules_state = rules.hash();

        for rule in &mut rules.iter_shared() {
            if let Some(mut rule) = rule.try_cast::<Rule>() {
                rule.bind_mut()._apply(action.share(), simulation.share());
            }
            else {
                godot_error!("Rulebook have a child that isn't a Rule");
            }
        }

        // if self.get_children(false).hash() != rules_state {
        //     godot_error!("Rules cannot be added or removed");
        // }
    }
}

#[godot_api]
impl NodeVirtual for Rulebook {
    fn init(base: Base<Node>) -> Self {
        Rulebook {
            base,
        }
    }

    fn ready(&mut self) {}
}
