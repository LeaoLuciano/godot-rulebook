use godot::prelude::*;


#[inline]
pub fn get_child_from<Type: Inherits<Node>>(node: Gd<Node>) -> Option<Gd<Type>> {
    let nodes = node.get_children(false);
    for node in nodes.iter_shared() {
        if let Some(node) = node.try_cast::<Type>() {
            return Some(node);
        }
    }
    None
}