use godot::{prelude::*};

pub mod world;
pub mod rule;
pub mod simulation;
pub mod util;

struct RulebookPlugin;

#[gdextension]
unsafe impl ExtensionLibrary for RulebookPlugin {
}
