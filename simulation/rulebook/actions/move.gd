extends Action
class_name MoveAction

var movement := MovementAttr.new()
var amount := AmountAttr.new()
var direction := DirectionAttr.new()
var target := TargetAttr.new()

func _init(displacement: Vector2, _target: Array[Script]):
	amount.value = displacement.length()
	if is_equal_approx(amount.value, 0):
		direction.value = Vector2()
	else:
		direction.value = displacement.normalized()
	
	target.value = _target
	
