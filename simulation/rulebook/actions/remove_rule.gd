extends Action
class_name RemoveRuleAction

var operation := RemoveAttr.new()
var rule := RuleAttr.new()

func _init(_rule: Rule):
	rule.value = _rule
	
