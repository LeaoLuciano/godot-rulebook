extends Rule

var operation := AddAttr.new()
var rule := RuleAttr.new()


func apply(simulation: Simulation):
	var rulebook = simulation.get_rulebook()
	rulebook.add_child(rule.value)
	rulebook.move_child(rule.value, 0)
	

