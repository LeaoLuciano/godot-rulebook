extends Rule

var operation := RemoveAttr.new()
var rule := RuleAttr.new()


func apply(simulation: Simulation):
	simulation.get_rulebook().remove_child(rule.value)

