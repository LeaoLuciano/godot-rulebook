extends Rule

var label := MovementAttr.new()
var direction := DirectionAttr.new()
var amount := AmountAttr.new()
var target := TargetAttr.new()


func apply(simulation: Simulation):
	var entities = simulation.get_world().query(target.value)
	
	for entity in entities:
		entity.position += amount.value * direction.value

