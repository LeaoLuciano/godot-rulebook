extends Rule

var label := MovementAttr.new()
var direction := DirectionAttr.new()


func apply(_simulation: Simulation):
	direction.value = direction.value.rotated(randf()*2*PI)

