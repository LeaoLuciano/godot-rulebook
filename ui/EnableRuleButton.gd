extends CheckButton

@onready var rule = get_child(0)

func _on_pressed():
	if button_pressed:
		remove_child(rule)
		Commands.new().apply_action(AddRuleAction.new(rule))
	else:
		Commands.new().apply_action(RemoveRuleAction.new(rule))
		add_child(rule)
		
